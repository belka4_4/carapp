package com.aliona.car.web.html;

import com.aliona.car.model.Owner;
import com.aliona.car.model.OwnerDto;
import com.aliona.service.owner.OwnerService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class OwnerControllerHtmlTest {

    private MockMvc mockMvc;
    private Owner owner, owner2;

    @Mock
    private OwnerService ownerService;

    @InjectMocks
    private OwnerControllerHtml ownerControllerHtml;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(ownerControllerHtml)
                .build();
        owner = new Owner("Volatova", "Inna", new GregorianCalendar(1983, 03, 15).getGregorianChange(), "2523677");
        owner.setId(0);
        owner2 = new Owner("Sergeev", "Sergei", new GregorianCalendar(1958, 04, 30).getGregorianChange(), "1717852");
        owner2.setId(1);
    }

    @Test
    public void getOwnersList() throws Exception {
        List<Owner> owners = new ArrayList<>();
        owners.add(owner);
        owners.add(owner2);
        when(ownerService.getAll()).thenReturn(owners);
        mockMvc.perform(get("/html/owners"))
                .andExpect(status().isOk())
                .andExpect(view().name("ownersList"));
        verify(ownerService, times(1)).getAll();
        verifyNoMoreInteractions(ownerService);
    }

    @Test
    public void getOwnerById() throws Exception {
        when(ownerService.getById(0)).thenReturn(owner);
        mockMvc.perform(get("/html/owner").param("ownerId", "0"))
                .andExpect(status().isOk())
                .andExpect(view().name("ownerPage"));
        verify(ownerService, times(1)).getById(0);
        verifyNoMoreInteractions(ownerService);
    }

    @Test
    public void addOwner() throws Exception {
        mockMvc.perform(
                post("/html/addowner")
                        .param("surname", "Lepeha")
                        .param("name", "Sergey")
                        .param("year", "1975")
                        .param("month", "10")
                        .param("day", "4")
                        .param("passport", "5678941"))
                .andExpect(status().isCreated())
                .andExpect(view().name("ownerPage"));
    }

    @Test
    public void updateOwner() throws Exception {
        doNothing().when(ownerService).update(owner);
        mockMvc.perform(
                put("/html/updateowner")
                        .param("ownerId", "0")
                        .param("surname", "Lepeha")
                        .param("name", "Sergey")
                        .param("year", "1975")
                        .param("month", "10")
                        .param("day", "4")
                        .param("passport", "5678941"))
                .andExpect(status().isOk())
                .andExpect(view().name("ownerPage"));

    }

    @Test(expected = Exception.class)
    public void updateOwner_404_not_found() throws Exception {
        when(ownerService.getById(owner.getId())).thenThrow(new Exception("oops"));
        mockMvc.perform(
                put("/html/updateowner")
                        .param("ownerId", "0")
                        .param("surname", "Lepeha")
                        .param("name", "Sergey")
                        .param("year", "1975")
                        .param("month", "10")
                        .param("day", "4")
                        .param("passport", "5678941"))
                .andExpect(status().isNotFound())
                .andExpect(view().name("error/notFound"));

        verify(ownerService, times(1)).getById(owner.getId());
        verifyNoMoreInteractions(ownerService);
    }

    @Test
    public void removeOwner() throws Exception {

        mockMvc.perform(delete("/html/removeowner").param("ownerId", "0"))
                .andExpect(status().isOk())
                .andExpect(view().name("ownersList"));

    }

    @Test(expected = Exception.class)
    public void removeCar_404_not_found() throws Exception {
        when(ownerService.getById(owner.getId())).thenThrow(new Exception("oops"));
        mockMvc.perform(delete("/html/removeowner").param("ownerId", "0"))
                .andExpect(status().isNotFound())
                .andExpect(view().name("error/notFound"));

        verify(ownerService, times(1)).getById(owner.getId());
        verifyNoMoreInteractions(ownerService);
    }

    @Test
    public void getAllOwnerDto() throws Exception {
        List<OwnerDto> ownerDtos = new ArrayList<>();
        ownerDtos.add(new OwnerDto(owner, 1));
        ownerDtos.add(new OwnerDto(owner2, 3));
        when(ownerService.getAllOwnerDto()).thenReturn(ownerDtos);
        mockMvc.perform(get("/html/ownerdto"))
                .andExpect(status().isOk())
                .andExpect(view().name("ownerDtosList"));
        verify(ownerService, times(1)).getAllOwnerDto();
        verifyNoMoreInteractions(ownerService);
    }
}