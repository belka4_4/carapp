package com.aliona.car.web.html;

import com.aliona.car.model.Car;
import com.aliona.car.model.Owner;
import com.aliona.service.car.CarService;
import com.aliona.service.owner.OwnerService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.junit.Assert.*;

public class CarControllerHtmlTest {

    private MockMvc mockMvc;
    private Car car, car2;

    @Mock
    private CarService carService;
    @Mock
    private OwnerService ownerService;

    @InjectMocks
    private CarControllerHtml carControllerHtml;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(carControllerHtml)
                .build();
        car = new Car("5682Ak1", "BMW e46", "7788gh2o32ltF63mP", "red", 1);
        car.setId(0);
        car2 = new Car("1880ko2", "toyota camry", "15h87P36rT01214P3", "white", 0);
        car2.setId(1);
    }

    @Test
    public void main() throws Exception {
        mockMvc.perform(get("/html/home"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    public void getCarsList() throws Exception {
        List<Car> cars = new ArrayList<>();
        cars.add(car);
        cars.add(car2);
        when(carService.getAll()).thenReturn(cars);
        mockMvc.perform(get("/html/cars"))
                .andExpect(status().isOk())
                .andExpect(view().name("carsList"));
        verify(carService, times(1)).getAll();
        verifyNoMoreInteractions(carService);
    }

    @Test
    public void getCarById() throws Exception {
        when(carService.getById(0)).thenReturn(car);
        mockMvc.perform(get("/html/car").param("carId", "0"))
                .andExpect(status().isOk())
                .andExpect(view().name("carPage"));
        verify(carService, times(1)).getById(0);
        verifyNoMoreInteractions(carService);
    }

    @Test
    public void addCar() throws Exception {
        when(ownerService.getById(car.getOwnerId())).thenReturn(new Owner());
        mockMvc.perform(
                post("/html/addcar")
                        .param("regNumber", "5682Ak1")
                        .param("model", "BMW e46")
                        .param("vin", "7788gh2o32ltF63mP")
                        .param("color", "red")
                        .param("ownerId", "1"))
                .andExpect(status().isCreated())
                .andExpect(view().name("carPage"));
    }

    @Test(expected = Exception.class)
    public void addCar_fail_404_not_found() throws Exception {
        when(ownerService.getById(car.getOwnerId())).thenThrow(new Exception("oops"));
        mockMvc.perform(
                post("/html/addcar")
                        .param("regNumber", "5682Ak1")
                        .param("model", "BMW e46")
                        .param("vin", "7788gh2o32ltF63mP")
                        .param("color", "red")
                        .param("ownerId", "20"))
                .andExpect(status().isBadRequest())
                .andExpect(view().name("error/notFound"));
        verifyNoMoreInteractions(carService);
    }

    @Test
    public void updateCar() throws Exception {
        when(ownerService.getById(car.getOwnerId())).thenReturn(new Owner());
        when(carService.getById(car.getId())).thenReturn(car);
        doNothing().when(carService).update(car);
        mockMvc.perform(
                put("/html/updatecar")
                        .param("carId", "0")
                        .param("regNumber", "5682Ak1")
                        .param("model", "BMW e46")
                        .param("vin", "7788gh2o32ltF63mP")
                        .param("color", "red")
                        .param("ownerId", "1"))
                .andExpect(status().isOk())
                .andExpect(view().name("carPage"));

        verify(carService, times(1)).getById(car.getId());
        verify(carService, times(1)).update(car);
        verifyNoMoreInteractions(carService);
    }

    @Test(expected = Exception.class)
    public void updateCar_404_not_found() throws Exception {
        when(carService.getById(car.getId())).thenThrow(new Exception("oops"));
        mockMvc.perform(
                put("/html/updatecar")
                        .param("carId", "0")
                        .param("regNumber", "5682Ak1")
                        .param("model", "BMW e46")
                        .param("vin", "7788gh2o32ltF63mP")
                        .param("color", "red")
                        .param("ownerId", "1"))
                .andExpect(status().isBadRequest())
                .andExpect(view().name("error/notFound"));

        verify(carService, times(1)).getById(car.getId());
        verifyNoMoreInteractions(carService);
    }

    @Test
    public void removeCar() throws Exception {

        when(carService.getById(0)).thenReturn(car);
        mockMvc.perform(delete("/html/removecar").param("carId", "0"))
                .andExpect(status().isOk())
                .andExpect(view().name("carsList"));

        verify(carService, times(1)).getById(car.getId());
        verify(carService, times(1)).remove(car);

    }

    @Test(expected = Exception.class)
    public void removeCar_404_not_found() throws Exception {
        when(carService.getById(car.getId())).thenThrow(new Exception("oops"));
        mockMvc.perform(delete("/html/removecar").param("carId", "0"))
                .andExpect(status().isBadRequest())
                .andExpect(view().name("error/notFound"));

        verify(carService, times(1)).getById(car.getId());
        verifyNoMoreInteractions(carService);
    }
}