package com.aliona.car.web.rest;

import com.aliona.car.model.Car;
import com.aliona.car.model.Owner;
import com.aliona.service.car.CarService;
import com.aliona.service.owner.OwnerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CarControllerRestTest {

    private MockMvc mockMvc;
    private Car car, car2;

    @Mock
    private CarService carService;
    @Mock
    private OwnerService ownerService;

    @InjectMocks
    private CarControllerRest carControllerRest;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(carControllerRest)
                .build();
        car = new Car("5682Ak1", "BMW e46", "7788gh2o32ltF63mP", "red", 1);
        car.setId(0);
        car2 = new Car("1880ko2", "toyota camry", "15h87P36rT01214P3", "white", 0);
        car2.setId(1);
    }

    @Test
    public void getCarsList() throws Exception {
        List<Car> cars = new ArrayList<>();
        cars.add(car);
        cars.add(car2);
        when(carService.getAll()).thenReturn(cars);
        mockMvc.perform(get("/rest/cars"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(0)))
                .andExpect(jsonPath("$[0].regNumber", is("5682Ak1")))
                .andExpect(jsonPath("$[1].id", is(1)))
                .andExpect(jsonPath("$[1].regNumber", is("1880ko2")));
        verify(carService, times(1)).getAll();
        verifyNoMoreInteractions(carService);
    }

    @Test
    public void getCarById() throws Exception {
        when(carService.getById(0)).thenReturn(car);
        mockMvc.perform(get("/rest/cars/{carId}", car.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", is(0)))
                .andExpect(jsonPath("$.regNumber", is("5682Ak1")));
        verify(carService, times(1)).getById(0);
        verifyNoMoreInteractions(carService);
    }

    @Test
    public void addCar() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        mockMvc.perform(
                post("/rest/cars")
                        .content(objectMapper.writeValueAsString(car))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test(expected = Exception.class)
    public void addCar_fail_404_not_found() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        when(ownerService.getById(car.getOwnerId())).thenThrow(new Exception("oops"));
        mockMvc.perform(
                post("/rest/cars")
                        .content(objectMapper.writeValueAsString(car))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
        verifyNoMoreInteractions(carService);
    }

    @Test
    public void updateCar() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        doNothing().when(carService).update(car);
        mockMvc.perform(
                put("/rest/cars")
                        .content(objectMapper.writeValueAsString(car))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(carService, times(1)).update(car);
        verifyNoMoreInteractions(carService);
    }

    @Test(expected = Exception.class)
    public void updateCar_404_not_found() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        when(carService.getById(car.getId())).thenThrow(new Exception("oops"));
        mockMvc.perform(
                put("/rest/cars")
                        .content(objectMapper.writeValueAsString(car)))
                .andExpect(status().isBadRequest());

        verify(carService, times(1)).getById(car.getId());
        verifyNoMoreInteractions(carService);
    }

    @Test
    public void removeCar() throws Exception {

        when(carService.getById(0)).thenReturn(car);
        mockMvc.perform(delete("/rest/cars/{carId}", car.getId()))
                .andExpect(status().isOk());

    }

    @Test(expected = Exception.class)
    public void removeCar_404_not_found() throws Exception {
        when(carService.getById(car.getId())).thenThrow(new Exception("oops"));
        mockMvc.perform(delete("/rest/cars/{carId}", car.getId()))
                .andExpect(status().isBadRequest());

        verify(carService, times(1)).getById(car.getId());
        verifyNoMoreInteractions(carService);
    }
}