package com.aliona.car.web.rest;

import com.aliona.car.model.Owner;
import com.aliona.car.model.OwnerDto;
import com.aliona.service.owner.OwnerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.junit.Assert.*;

public class OwnerControllerRestTest {

    private MockMvc mockMvc;
    private Owner owner, owner2;

    @Mock
    private OwnerService ownerService;

    @InjectMocks
    private OwnerControllerRest ownerControllerRest;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(ownerControllerRest)
                .build();
        owner = new Owner("Volatova", "Inna", new GregorianCalendar(1983, 03, 15).getGregorianChange(), "2523677");
        owner.setId(0);
        owner2 = new Owner("Sergeev", "Sergei", new GregorianCalendar(1958, 04, 30).getGregorianChange(), "1717852");
        owner2.setId(1);
    }

    @Test
    public void getOwnersList() throws Exception {
        List<Owner> owners = new ArrayList<>();
        owners.add(owner);
        owners.add(owner2);
        when(ownerService.getAll()).thenReturn(owners);
        mockMvc.perform(get("/rest/owners"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(0)))
                .andExpect(jsonPath("$[0].passport", is("2523677")))
                .andExpect(jsonPath("$[1].id", is(1)))
                .andExpect(jsonPath("$[1].passport", is("1717852")));
        verify(ownerService, times(1)).getAll();
        verifyNoMoreInteractions(ownerService);
    }

    @Test
    public void getOwnerById() throws Exception {
        when(ownerService.getById(0)).thenReturn(owner);
        mockMvc.perform(get("/rest/owners/{ownerId}", owner.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", is(0)))
                .andExpect(jsonPath("$.passport", is("2523677")));
        verify(ownerService, times(1)).getById(0);
        verifyNoMoreInteractions(ownerService);
    }

    @Test
    public void addOwner() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        mockMvc.perform(
                post("/rest/owners")
                        .content(objectMapper.writeValueAsString(owner))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void updateOwner() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        when(ownerService.getById(owner.getId())).thenReturn(owner);
        doNothing().when(ownerService).update(owner);
        mockMvc.perform(
                put("/rest/owners")
                        .content(objectMapper.writeValueAsString(owner))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(ownerService, times(1)).update(owner);
        verifyNoMoreInteractions(ownerService);
    }

    @Test(expected = Exception.class)
    public void updateOwner_404_not_found() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        when(ownerService.getById(owner.getId())).thenThrow(new Exception("oops"));
        mockMvc.perform(
                put("/rest/owners")
                        .content(objectMapper.writeValueAsString(owner)))
                .andExpect(status().isBadRequest());

        verify(ownerService, times(1)).getById(owner.getId());
        verifyNoMoreInteractions(ownerService);
    }

    @Test
    public void removeOwner() throws Exception {

        when(ownerService.getById(0)).thenReturn(owner);
        mockMvc.perform(delete("/rest/owners/{ownerId}", owner.getId()))
                .andExpect(status().isOk());

    }

    @Test(expected = Exception.class)
    public void removeCar_404_not_found() throws Exception {
        when(ownerService.getById(owner.getId())).thenThrow(new Exception("oops"));
        mockMvc.perform(delete("/rest/owners/{ownerId}", owner.getId()))
                .andExpect(status().isBadRequest());

        verify(ownerService, times(1)).getById(owner.getId());
        verifyNoMoreInteractions(ownerService);
    }

    @Test
    public void getAllOwnerDto() throws Exception {
        List<OwnerDto> ownerDtos = new ArrayList<>();
        ownerDtos.add(new OwnerDto(owner, 1));
        ownerDtos.add(new OwnerDto(owner2, 3));
        when(ownerService.getAllOwnerDto()).thenReturn(ownerDtos);
        mockMvc.perform(get("/rest/ownerDtos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].owner.id", is(0)))
                .andExpect(jsonPath("$[0].count", is(1)))
                .andExpect(jsonPath("$[1].owner.id", is(1)))
                .andExpect(jsonPath("$[1].count", is(3)));
        verify(ownerService, times(1)).getAllOwnerDto();
        verifyNoMoreInteractions(ownerService);
    }
}