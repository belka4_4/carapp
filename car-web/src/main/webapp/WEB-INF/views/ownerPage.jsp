<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
    <body>
        <a href="home">Home</a><br>
        <form action="updateowner" method="PUT" modelAttribute="owner">
        <table>
        <tr>
            <td><label path="ownerId">id:</label></td>
            <td><input type="text" name="ownerId" value="${owner.id}"><td>
        </tr>
        <tr>
            <td><label path="surname">surname:</label></td>
            <td><input type="text" name="surname" value="${owner.surname}"></td>
        </tr>
        <tr>
            <td><label path="name">name:</label></td>
            <td><input type="text" name="name"value="${owner.name}"></td>
        </tr>
        <tr>
            <td><label path="birthDate">birthDate</label></td>
            <td><label path="day">day:</label>
                <input type="text" name="day"value="${bDay}">
                <label path="month">month:</label>
                <input type="text" name="month"value="${bMonth}">
                <label path="year">year:</label>
                <input type="text" name="year"value="${bYear}"></td>
        </tr>
        <tr>
            <td><label path="passport">passport:</label></td>
            <td><input type="text" name="passport"value="${owner.passport}"></td>
         </tr>
        </table>
        <input type="submit" name="html/updateowner" value="update">
        </form>
</body>
</html>