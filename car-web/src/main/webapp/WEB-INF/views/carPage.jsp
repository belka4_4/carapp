<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
    <body>
        <a href="home">Home</a><br>
        <form action="updatecar" method="PUT" modelAttribute="car">
        <table>
        <tr>
            <td><label path="carId">id:</label></td>
            <td><input type="text" name="carId" value="${car.id}" /><td>
        </tr>
        <tr>
            <td><label path="regNumber">regNumber:</label></td>
            <td><input type="text" name="regNumber" value="${car.regNumber}" /><td>
        </tr>
        <tr>
            <td><label path="model">model:</label></td>
            <td><input type="text" name="model" value="${car.model}" /><td>
        </tr>
        <tr>
            <td><label path="vin">vin:</label></td>
            <td><input type="text" name="vin" value="${car.vin}" /><td>
        </tr>
        <tr>
            <td><label path="color">color:</label></td>
            <td><input type="text" name="color" value="${car.color}" /><td>
        </tr>
        <tr>
            <td><label path="ownerId">ownerId:</label></td>
            <td><input type="text" name="ownerId" value="${car.ownerId}" /><td>
        </tr>
        </table>
        <input type="submit" name="html/updatecar" value="update">
        </form>
</body>
</html>