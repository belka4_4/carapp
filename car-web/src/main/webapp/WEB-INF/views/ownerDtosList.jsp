<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
    <body>
        <style type="text/css">
        TABLE {
        width: 300px;
        border-collapse: collapse;
        }
        TD, TH {
        padding: 3px;
        border: 1px solid black;
        }
        TH {
        background: #b0e0e6;
        }
        </style>
        <a href="home">Home</a><br>
        <p>
        <form:form method="get" modelAttribute="ownerDtos">
        <h1> List of owners by cars count </h1>
        <ul>
            <table>
                    <th>ownerId</th>
                    <th>surname</th>
                    <th>name</th>
                    <th>carsCount</th>
                <c:forEach items="${ownerDtos}" var="ob">
                    <tr>
                         <td><a href='<spring:url value="/html/owner" ><spring:param name="ownerId" value="${ob.owner.id}" /> </spring:url>'>${ob.owner.id}</a></td>
                         <td>${ob.owner.surname}</td>
                         <td>${ob.owner.name}</td>
                         <td>${ob.count}</td>
                     </tr>
                </c:forEach>
            </table>
        </ul>
        </form:form>
    </body>
</html>