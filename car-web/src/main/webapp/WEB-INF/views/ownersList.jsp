<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
    <body>
        <style type="text/css">
        TABLE {
        width: 300px;
        border-collapse: collapse;
        }
        TD, TH {
        padding: 3px;
        border: 1px solid black;
        }
        TH {
        background: #b0e0e6;
        }
        </style>
        <a href="home">Home</a><br>
        <p>
        <form:form method="get" modelAttribute="owners">
        <h1> List of owners </h1>
        <ul>
            <table>
                    <th>ownerId</th>
                    <th>surname</th>
                    <th>name</th>
                    <th>birthDate</th>
                    <th>passport</th>
                <c:forEach items="${owners}" var="ob">
                    <tr>
                         <td><a href='<spring:url value="/html/owner" ><spring:param name="ownerId" value="${ob.id}" /> </spring:url>'>${ob.id}</a></td>
                         <td>${ob.surname}</td>
                         <td>${ob.name}</td>
                         <fmt:formatDate value="${ob.birthDate}" var="dateString" pattern="dd/MM/yyyy" />
                         <td>${dateString}</td>
                         <td>${ob.passport}</td>
                         <td><a href='<spring:url value="/html/removeowner" ><spring:param name="ownerId" value="${ob.id}" /> </spring:url>'>del</a></td>
                     </tr>
                </c:forEach>
            </table>
        </ul>
        </form:form>
        <p>
            <form action="addowner" method="POST">
            <table>
                    <tr>
                        <td><label path="surname">surname:</label></td>
                        <td><input type="text" name="surname"></td>
                    </tr>
                    <tr>
                        <td><label path="name">name:</label></td>
                        <td><input type="text" name="name"></td>
                    </tr>
                    <tr>
                        <td><label path="birthDate">birthDate</label></td>
                        <td><label path="day">day:</label>
                            <input type="text" name="day"/>
                            <label path="month">month:</label>
                            <input type="text" name="month"/>
                            <label path="year">year:</label>
                            <input type="text" name="year"/></td>
                    </tr>
                    <tr>
                        <td><label path="passport">passport:</label></td>
                        <td><input type="text" name="passport"/></td>
                    </tr>
                    </table>
             <input type="submit" name="html/addowner" value="add">
             </form>
    </body>
</html>