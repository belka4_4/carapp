<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
    <body>
        <style type="text/css">
        TABLE {
        width: 300px;
        border-collapse: collapse;
        }
        TD, TH {
        padding: 3px;
        border: 1px solid black;
        }
        TH {
        background: #b0e0e6;
        }
        </style>
        <a href="home">Home</a><br>
        <p>
        <form:form method="get" modelAttribute="cars">
        <h1> List of cars </h1>
        <ul>
            <table>
                    <th>carId</th>
                    <th>regNumber</th>
                    <th>model</th>
                    <th>vin</th>
                    <th>color</th>
                    <th>ownerId</th>
                <c:forEach items="${cars}" var="ob">
                    <tr>
                         <td><a href='<spring:url value="/html/car" ><spring:param name="carId" value="${ob.id}" /> </spring:url>'>${ob.id}</a></td>
                         <td>${ob.regNumber}</td>
                         <td>${ob.model}</td>
                         <td>${ob.vin}</td>
                         <td>${ob.color}</td>
                         <td><a href='<spring:url value="/html/owner" ><spring:param name="ownerId" value="${ob.ownerId}" /> </spring:url>'>${ob.ownerId}</a></td>
                         <td><a href='<spring:url value="/html/removecar" ><spring:param name="carId" value="${ob.id}" /> </spring:url>'>del</a></td>
                     </tr>
                </c:forEach>
            </table>
        </ul>
        </form:form>
        <p>
            <form action="addcar" method="POST">
            <table>
                    <tr>
                        <td><label path="regNumber">regNumber:</label></td>
                        <td><input type="text" name="regNumber"/><td>
                    </tr>
                    <tr>
                        <td><label path="model">model:</label></td>
                        <td><input type="text" name="model"/><td>
                    </tr>
                    <tr>
                        <td><label path="vin">vin:</label></td>
                        <td><input type="text" name="vin"/><td>
                    </tr>
                    <tr>
                        <td><label path="color">color:</label></td>
                        <td><input type="text" name="color"/><td>
                    </tr>
                    <tr>
                        <td><label path="ownerId">ownerId:</label></td>
                        <td><input type="text" name="ownerId"/><td>
                    </tr>
                    </table>
             <input type="submit" name="html/addcar" value="add">
             </form>
    </body>
</html>