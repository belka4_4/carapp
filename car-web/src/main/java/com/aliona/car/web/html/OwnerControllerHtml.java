package com.aliona.car.web.html;

import com.aliona.car.model.Owner;
import com.aliona.car.model.OwnerDto;
import com.aliona.service.owner.OwnerService;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/html")
public class OwnerControllerHtml {

    @Autowired
    OwnerService ownerService;
    private static final Logger LOGGER = LogManager.getLogger();

    @RequestMapping("/owners")
    public ModelAndView getOwnersList() {
        LOGGER.info("get ownersList");
        List<Owner> owners = ownerService.getAll();
        LOGGER.info("owners.size = " + owners.size());
        ModelAndView view = new ModelAndView("ownersList", "owners", owners);
        return view;
    }

    @RequestMapping("/owner")
    public ModelAndView getOwnerById(@RequestParam("ownerId") Integer id) {
        LOGGER.info("get owner id = {}", id);
        Owner owner = ownerService.getById(id);
        Date birthDate = owner.getBirthDate();
        SimpleDateFormat formatNowDay = new SimpleDateFormat("dd");
        SimpleDateFormat formatNowMonth = new SimpleDateFormat("MM");
        SimpleDateFormat formatNowYear = new SimpleDateFormat("YYYY");

        String bDay = formatNowDay.format(birthDate);
        String bMonth = formatNowMonth.format(birthDate);
        String bYear = formatNowYear.format(birthDate);

        Map<String, Object> modelMap = new TreeMap<>();
        modelMap.put("owner", owner);
        modelMap.put("owner", owner);
        modelMap.put("bDay", bDay);
        modelMap.put("bMonth", bMonth);
        modelMap.put("bYear", bYear);
        ModelAndView view = new ModelAndView("ownerPage", modelMap);
        return view;
    }

    @RequestMapping("/addowner")
    public ModelAndView addOwner(@RequestParam("surname") String surname,
                                 @RequestParam("name") String name,
                                 @RequestParam("year") int year,
                                 @RequestParam("month") int month,
                                 @RequestParam("day") int day,
                                 @RequestParam("passport") String passport) {

        LOGGER.info("add owner surname = {}, name = {}, year = {}, month = {}, day = {},  passport = {}",
                surname, name, year, month, day, passport);
        Owner owner = new Owner(surname, name, new GregorianCalendar(year, month - 1, day).getTime(), passport);
        ownerService.add(owner);

        Map<String, Object> modelMap = new TreeMap<>();
        modelMap.put("owner", owner);
        modelMap.put("bDay", day);
        modelMap.put("bMonth", month);
        modelMap.put("bYear", year);
        ModelAndView view = new ModelAndView("ownerPage", modelMap);
        view.setStatus(HttpStatus.CREATED);
        return view;
    }

    @RequestMapping("/updateowner")
    public ModelAndView updateOwner(@RequestParam("ownerId") Integer id,
                                    @RequestParam("surname") String surname,
                                    @RequestParam("name") String name,
                                    @RequestParam("year") int year,
                                    @RequestParam("month") int month,
                                    @RequestParam("day") int day,
                                    @RequestParam("passport") String passport) {

        LOGGER.info("update owner ownerId = {}, surname = {}, name = {}, year = {}, month = {}, day = {},  passport = {}",
                id, surname, name, year, month, day, passport);
        Owner owner = new Owner();
        owner.setId(id);
        owner.setSurname(surname);
        owner.setName(name);
        owner.setBirthDate(new GregorianCalendar(year, month - 1, day).getTime());
        owner.setPassport(passport);
        ownerService.update(owner);

        Map<String, Object> modelMap = new TreeMap<>();
        modelMap.put("owner", owner);
        modelMap.put("owner", owner);
        modelMap.put("bDay", day);
        modelMap.put("bMonth", month);
        modelMap.put("bYear", year);
        ModelAndView view = new ModelAndView("ownerPage", modelMap);
        return view;
    }

    @RequestMapping("/removeowner")
    public ModelAndView removeOwner(@RequestParam("ownerId") Integer id) {
        Owner owner = new Owner();
        owner.setId(id);
        LOGGER.info("remove owner id = {}", id);
        ownerService.remove(owner);
        List<Owner> owners = ownerService.getAll();
        ModelAndView view = new ModelAndView("ownersList", "owners", owners);
        return view;
    }

    @RequestMapping("/ownerdto")
    public ModelAndView getAllOwnerDto() {
        LOGGER.info("get ownerDtos");
        List<OwnerDto> ownerDtos = ownerService.getAllOwnerDto();
        LOGGER.info("ownerDtos.size = " + ownerDtos.size());
        ModelAndView view = new ModelAndView("ownerDtosList", "ownerDtos", ownerDtos);
        return view;
    }

    @ExceptionHandler({Exception.class})
    public ModelAndView handleException() {
        ModelAndView view = new ModelAndView("error/notFound", "textError", "oops");
        view.setStatus(HttpStatus.BAD_REQUEST);
        return view;
    }
}
