package com.aliona.car.web.rest;

import com.aliona.car.model.Owner;
import com.aliona.car.model.OwnerDto;
import org.springframework.stereotype.Controller;
import com.aliona.service.owner.OwnerService;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/rest")
public class OwnerControllerRest {
    @Autowired
    OwnerService ownerService;
    private static final Logger LOGGER = LogManager.getLogger();

    @RequestMapping(method = RequestMethod.GET, path = "/owners")
    public @ResponseBody
    ResponseEntity getOwnersList() {

        LOGGER.info("get ownersList");
        List<Owner> owners = ownerService.getAll();
        LOGGER.info("owners.size = " + owners.size());
        return new ResponseEntity<>(owners, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/owners/{ownerId}")
    public @ResponseBody
    ResponseEntity getOwnerById(@PathVariable("ownerId") Integer id) {

        LOGGER.info("get owner id = {}", id);
        Owner owner = ownerService.getById(id);
        return new ResponseEntity<>(owner, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/owners")
    public @ResponseBody
    ResponseEntity addOwner(@RequestBody Owner owner) {

        LOGGER.info("add owner {}", owner);
        ownerService.add(owner);
        return new ResponseEntity<>(owner, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/owners")
    public @ResponseBody
    ResponseEntity updateOwner(@RequestBody Owner owner) {

        LOGGER.info("update owner {}", owner);
        ownerService.update(owner);
        return new ResponseEntity<>(owner, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/owners/{ownerId}")
    public @ResponseBody
    ResponseEntity removeOwner(@PathVariable("ownerId") Integer id) {

        Owner owner = new Owner();
        owner.setId(id);
        LOGGER.info("remove owner id = {}", id);
        ownerService.remove(owner);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/ownerDtos")
    public @ResponseBody
    ResponseEntity getAllOwnerDto() {

        LOGGER.info("get ownerDtos");
        List<OwnerDto> ownerDtos = ownerService.getAllOwnerDto();
        LOGGER.info("ownerDtos.size = " + ownerDtos.size());
        return new ResponseEntity<>(ownerDtos, HttpStatus.OK);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity handleException(Exception ex) {
        Map<String, Object> map = new HashMap();
        map.put("error", true);
        map.put("message", ex.getMessage());
        return new ResponseEntity(map, HttpStatus.BAD_REQUEST);
    }
}
