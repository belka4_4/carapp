package com.aliona.car.web.html;

import com.aliona.car.model.Car;
import com.aliona.service.car.CarService;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/html")
public class CarControllerHtml {

    @Autowired
    CarService carService;
    private static final Logger LOGGER = LogManager.getLogger();

    @RequestMapping("/home")
    public ModelAndView main() {
        LOGGER.info("home");
        ModelAndView view = new ModelAndView("index");
        return view;
    }

    @RequestMapping("/cars")
    public ModelAndView getCarsList() {
        LOGGER.info("get carsList");
        List<Car> cars = carService.getAll();
        LOGGER.info("cars.size = " + cars.size());
        ModelAndView view = new ModelAndView("carsList", "cars", cars);
        return view;
    }

    @RequestMapping("/car")
    public ModelAndView getCarById(@RequestParam("carId") Integer id) {
        LOGGER.info("get car id = {}", id);
        Car car = carService.getById(id);
        if (car == null) {
            LOGGER.error("car with id = {} is not exist", id);
            ModelAndView view = new ModelAndView("error/notFound", "textError", "car with id = " + id + " is not exist");
            view.setStatus(HttpStatus.NOT_FOUND);
            return view;
        }
        ModelAndView view = new ModelAndView("carPage", "car", car);
        return view;
    }

    @RequestMapping("/addcar")
    public ModelAndView addCar(@RequestParam("regNumber") String regNumber,
                               @RequestParam("model") String model,
                               @RequestParam("vin") String vin,
                               @RequestParam("color") String color,
                               @RequestParam("ownerId") Integer ownerId) {

        Car car = new Car(regNumber, model, vin, color, ownerId);
        LOGGER.info("add car regNumber = {}, model = {}, vin = {}, color = {}, ownerId = {}",
                regNumber, model, vin, color, ownerId);
        carService.add(car);

        ModelAndView view = new ModelAndView("carPage", "car", car);
        view.setStatus(HttpStatus.CREATED);
        return view;
    }

    @RequestMapping("/updatecar")
    public ModelAndView updateCar(@RequestParam("carId") Integer id,
                                  @RequestParam("regNumber") String regNumber,
                                  @RequestParam("model") String model,
                                  @RequestParam("vin") String vin,
                                  @RequestParam("color") String color,
                                  @RequestParam("ownerId") Integer ownerId) {

        Car car = carService.getById(id);
        if (car == null) {
            LOGGER.error("car with id = {} is not exist", id);
            ModelAndView view = new ModelAndView("error/notFound", "textError", "car with id = " + id + " is not exist");
            view.setStatus(HttpStatus.NOT_FOUND);
            return view;
        }

        car.setRegNumber(regNumber);
        car.setModel(model);
        car.setVin(vin);
        car.setColor(color);
        car.setOwnerId(ownerId);
        LOGGER.info("update car carId = {}, regNumber = {}, model = {}, vin = {}, color = {}, ownerId = {}",
                id, regNumber, model, vin, color, ownerId);
        carService.update(car);

        ModelAndView view = new ModelAndView("carPage", "car", car);
        return view;
    }

    @RequestMapping("/removecar")
    public ModelAndView removeCar(@RequestParam("carId") Integer id) {
        Car car = carService.getById(id);
        LOGGER.info("remove car id = {}", id);
        carService.remove(car);
        List<Car> cars = carService.getAll();
        ModelAndView view = new ModelAndView("carsList", "cars", cars);
        view.setStatus(HttpStatus.OK);
        return view;
    }

    @ExceptionHandler({Exception.class})
    public ModelAndView handleException() {
        ModelAndView view = new ModelAndView("error/notFound", "textError", "oops");
        view.setStatus(HttpStatus.BAD_REQUEST);
        return view;
    }
}
