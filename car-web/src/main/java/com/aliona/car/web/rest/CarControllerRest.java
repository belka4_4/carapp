package com.aliona.car.web.rest;

import com.aliona.car.model.Car;
import com.aliona.service.car.CarService;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/rest")
public class CarControllerRest {
    @Autowired
    CarService carService;
    private static final Logger LOGGER = LogManager.getLogger();

    @RequestMapping(method = RequestMethod.GET, path = "/cars")
    public @ResponseBody
    ResponseEntity getCarsList() {

        LOGGER.info("get carsList");
        List<Car> cars = carService.getAll();
        LOGGER.info("cars.size = " + cars.size());
        return new ResponseEntity<>(cars, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/cars/{carId}")
    public @ResponseBody
    ResponseEntity getCarById(@PathVariable("carId") Integer id) {

        LOGGER.info("get car id = {}", id);
        Car car = carService.getById(id);
        return new ResponseEntity<>(car, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/cars")
    public @ResponseBody
    ResponseEntity addCar(@RequestBody Car car) {

        LOGGER.info("add car {}", car);
        carService.add(car);
        return new ResponseEntity<>(car, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/cars")
    public @ResponseBody
    ResponseEntity updateCar(@RequestBody Car car) {

        LOGGER.info("update car {}", car);
        carService.update(car);
        return new ResponseEntity<>(car, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/cars/{carId}")
    public @ResponseBody
    ResponseEntity removeCar(@PathVariable("carId") Integer id) {

        Car car = new Car();
        car.setId(id);
        LOGGER.info("remove car id = {}", id);
        carService.remove(car);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity handleException(Exception ex) {
        Map<String, Object> map = new HashMap();
        map.put("error", true);
        map.put("message", ex.getMessage());
        return new ResponseEntity(map, HttpStatus.BAD_REQUEST);
    }
}
