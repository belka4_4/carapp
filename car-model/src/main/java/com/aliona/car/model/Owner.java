package com.aliona.car.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class Owner {

    private Integer id;
    private String surname;
    private String name;
    private Date birthDate;
    private String passport;

    public Owner() {
    }

    public Owner(String surname, String name, Date birthDate, String passport) {
        this.surname = surname;
        this.name = name;
        this.birthDate = birthDate;
        this.passport = passport;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    @Override
    public boolean equals(Object o) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Owner owner = (Owner) o;
        return Objects.equals(id, owner.id) &&
                Objects.equals(surname, owner.surname) &&
                Objects.equals(name, owner.name) &&
                Objects.equals(sdf.format(birthDate), sdf.format(owner.birthDate)) &&
                Objects.equals(passport, owner.passport);
    }

    @Override
    public String toString() {
        return "Owner{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", birthDate=" + birthDate +
                ", passport='" + passport + '\'' +
                '}';
    }
}
