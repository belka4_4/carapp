package com.aliona.car.model;

public class OwnerDto {

    private Owner owner;
    private Integer count;

    public OwnerDto(Owner owner, Integer count) {
        this.owner = owner;
        this.count = count;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
