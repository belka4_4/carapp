package com.aliona.car.model;

import java.util.Objects;

public class Car {

    private Integer id;
    private String regNumber;
    private String model;
    private String vin;
    private String color;
    private Integer ownerId;

    public Car() {
    }

    public Car(String regNumber, String model, String vin, String color, Integer ownerId) {
        this.regNumber = regNumber;
        this.model = model;
        this.vin = vin;
        this.color = color;
        this.ownerId = ownerId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerID) {
        this.ownerId = ownerID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(id, car.id) &&
                Objects.equals(regNumber, car.regNumber) &&
                Objects.equals(model, car.model) &&
                Objects.equals(vin, car.vin) &&
                Objects.equals(color, car.color) &&
                Objects.equals(ownerId, car.ownerId);
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", regNumber='" + regNumber + '\'' +
                ", model='" + model + '\'' +
                ", vin='" + vin + '\'' +
                ", color='" + color + '\'' +
                ", ownerId=" + ownerId +
                '}';
    }
}
