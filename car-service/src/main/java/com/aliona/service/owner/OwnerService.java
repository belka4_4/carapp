package com.aliona.service.owner;

import com.aliona.car.model.Owner;
import com.aliona.car.model.OwnerDto;

import java.util.List;

public interface OwnerService {

    void add(Owner owner);

    Owner getById(Integer id);

    List<Owner> getAll();

    void update(Owner owner);

    void remove(Owner owner);

    List<OwnerDto> getAllOwnerDto();

}
