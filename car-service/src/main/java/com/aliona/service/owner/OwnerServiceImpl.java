package com.aliona.service.owner;

        import com.aliona.car.dao.OwnerDao;
        import com.aliona.car.model.Owner;
        import com.aliona.car.model.OwnerDto;
        import org.springframework.stereotype.Service;

        import java.util.List;

@Service("ownerService")
public class OwnerServiceImpl implements OwnerService {

    private OwnerDao ownerDao;

    public OwnerServiceImpl(OwnerDao ownerDao) {
        this.ownerDao = ownerDao;
    }

    @Override
    public void add(Owner owner) {
        ownerDao.add(owner);
    }

    @Override
    public Owner getById(Integer id) {
        return ownerDao.getById(id);
    }

    @Override
    public List<Owner> getAll() {
        return ownerDao.getAll();
    }

    @Override
    public void update(Owner owner) {
        ownerDao.update(owner);
    }

    @Override
    public void remove(Owner owner) {
        ownerDao.remove(owner);
    }

    @Override
    public List<OwnerDto> getAllOwnerDto() {
        return ownerDao.getAllOwnerDto();
    }
}
