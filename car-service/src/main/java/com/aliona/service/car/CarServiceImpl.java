package com.aliona.service.car;

import com.aliona.car.dao.CarDao;
import com.aliona.car.model.Car;

import java.util.List;

public class CarServiceImpl implements CarService {

    private CarDao carDao;

    public CarServiceImpl(CarDao carDao) {
        this.carDao = carDao;
    }

    @Override
    public void add(Car car) {
        carDao.add(car);
    }

    @Override
    public Car getById(Integer id) {
        return carDao.getById(id);
    }

    @Override
    public List<Car> getAll() {
        return carDao.getAll();
    }

    @Override
    public void update(Car car) {
        carDao.update(car);
    }

    @Override
    public void remove(Car car) {
        carDao.remove(car);
    }
}
