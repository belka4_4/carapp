package com.aliona.service;

import com.aliona.car.dao.OwnerDao;
import com.aliona.car.model.Owner;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.aliona.service.owner, com.aliona.car.dao, com.aliona.car.model"})
public class AppConfig {

    @Bean(name = "ownerDao")
    public OwnerDao getOwnerDao() {
        return Mockito.mock(OwnerDao.class);
    }

}
