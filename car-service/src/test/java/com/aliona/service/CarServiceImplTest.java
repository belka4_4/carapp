package com.aliona.service;

import com.aliona.car.dao.CarDao;
import com.aliona.car.model.Car;
import com.aliona.service.car.CarService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:context.xml")
public class CarServiceImplTest {

    @Autowired
    private CarService carService;
    @Autowired
    private CarDao mockCarDao;

    @Test
    public void add() {
        Car car = new Car();
        carService.add(car);
        verify(mockCarDao).add(car);
    }

    @Test
    public void getById() {
        Car car = new Car();
        carService.getById(car.getId());
        verify(mockCarDao).getById(car.getId());
    }

    @Test
    public void getAll() {
        Car car = new Car();
        carService.getAll();
        verify(mockCarDao).getAll();
    }

    @Test
    public void update() {
        Car car = new Car();
        carService.update(car);
        verify(mockCarDao).update(car);
    }

    @Test
    public void remove() {
        Car car = new Car();
        carService.remove(car);
        verify(mockCarDao).remove(car);
    }
}