package com.aliona.service;

import com.aliona.car.dao.OwnerDao;
import com.aliona.car.model.Owner;
import com.aliona.service.owner.OwnerServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
public class OwnerServiceImplTest {

    @Autowired
    private OwnerDao mockOwnerDao;
    @Autowired
    private OwnerServiceImpl ownerService;

    @Test
    public void add() {
        Owner owner = new Owner();
        ownerService.add(owner);
        verify(mockOwnerDao).add(owner);
    }

    @Test
    public void getById() {
        Owner owner = new Owner();
        ownerService.getById(owner.getId());
        verify(mockOwnerDao).getById(owner.getId());
    }

    @Test
    public void getAll() {
        ownerService.getAll();
        verify(mockOwnerDao).getAll();
    }

    @Test
    public void update() {
        Owner owner = new Owner();
        ownerService.update(owner);
        verify(mockOwnerDao).update(owner);
    }

    @Test
    public void removeById() {
        Owner owner = new Owner();
        ownerService.remove(owner);
        verify(mockOwnerDao).remove(owner);
    }

    @Test
    public void getAllOwnerDto() {
        ownerService.getAllOwnerDto();
        verify(mockOwnerDao).getAllOwnerDto();
    }
}