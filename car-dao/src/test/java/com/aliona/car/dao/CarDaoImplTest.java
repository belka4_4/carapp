package com.aliona.car.dao;

import com.aliona.car.map.CarMapper;
import com.aliona.car.model.Car;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:context.xml")
public class CarDaoImplTest {

    private Car car, car2, car3;

    @Autowired
    private CarDaoImpl carDao;
    @Autowired
    private CarMapper carMapper;

    @Before
    public void setUp() {
        car = new Car("5682Ak1", "BMW e46", "7788gh2o32ltF63mP", "red", 1);
        car2 = new Car("1880ko2", "toyota camry", "15h87P36rT01214P3", "white", 0);
        car3 = new Car("5150HH3", "lada", "93799j92PoTit07Oi", "blue", 2);
    }

    @Test
    public void add_getById() {
        carDao.add(car2);
        Car actual = carDao.getById(car2.getId());
        Assert.assertTrue(car2.equals(actual));
    }

    @Test
    public void getAll() {
        List<Car> cars = carDao.getAll();
        int beforeSize = cars.size();
        carDao.add(car);
        carDao.add(car2);
        carDao.add(car3);
        cars = carDao.getAll();
        Assert.assertTrue(cars.size() == beforeSize + 3);
        Assert.assertTrue(car.equals(cars.get(beforeSize)));
        Assert.assertTrue(cars.get(beforeSize + 2).getId() == car3.getId());
    }

    @Test
    public void update() {
        List<Car> cars = carDao.getAll();
        carDao.add(car);
        car.setVin("1234pO36M811Pth37");
        carDao.update(car);
        Car actual = carDao.getById(car.getId());
        Assert.assertTrue(actual.getVin().equals("1234pO36M811Pth37"));
        carDao.remove(car);
    }

    @Test
    public void remove() {
        List<Car> cars = carDao.getAll();
        int beforeSize = cars.size();
        carDao.add(car);
        carDao.add(car2);
        carDao.add(car3);
        cars = carDao.getAll();
        Assert.assertTrue(cars.size() == beforeSize + 3);
        Assert.assertTrue(cars.contains(car));
        carDao.remove(car);
        cars = carDao.getAll();
        Assert.assertTrue(cars.size() == beforeSize + 2);
        Assert.assertFalse(cars.contains(car));
    }
}