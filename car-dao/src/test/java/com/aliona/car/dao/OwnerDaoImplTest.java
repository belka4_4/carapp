package com.aliona.car.dao;

import com.aliona.car.map.OwnerDtoMapper;
import com.aliona.car.map.OwnerMapper;
import com.aliona.car.model.Car;
import com.aliona.car.model.Owner;
import com.aliona.car.model.OwnerDto;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.GregorianCalendar;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:context.xml")
public class OwnerDaoImplTest {

    private Owner owner, owner2, owner3;
    private Car car, car2, car3;

    @Autowired
    private OwnerDaoImpl ownerDao;
    @Autowired
    private OwnerMapper ownerMapper;
    @Autowired
    private OwnerDtoMapper ownerDtoMapper;
    @Autowired
    private CarDao carDao;

    @Before
    public void setUp() {
        owner = new Owner("Volatova", "Inna", new GregorianCalendar(1983, 03, 15).getGregorianChange(), "2523677");
        owner2 = new Owner("Sergeev", "Sergei", new GregorianCalendar(1958, 04, 30).getGregorianChange(), "1717852");
        owner3 = new Owner("Rudnev", "Semen", new GregorianCalendar(1972, 10, 14).getGregorianChange(), "5656632");
        car = new Car("5682Ak1", "BMW e46", "7788gh2o32ltF63mP", "red", 2);
        car2 = new Car("1880ko2", "toyota camry", "15h87P36rT01214P3", "white", 1);
        car3 = new Car("5150HH3", "lada", "93799j92PoTit07Oi", "blue", 2);
    }

    @Test
    public void add_getById() {
        ownerDao.add(owner);
        Owner actual = ownerDao.getById(owner.getId());
        Assert.assertTrue(owner.equals(actual));
    }

    @Test
    public void getAll() {
        List<Owner> owners = ownerDao.getAll();
        int beforeSize = owners.size();
        ownerDao.add(owner);
        ownerDao.add(owner2);
        ownerDao.add(owner3);
        owners = ownerDao.getAll();
        Assert.assertTrue(owners.size() == 3 + beforeSize);
        Assert.assertTrue(owner.equals(owners.get(beforeSize)));
        Assert.assertTrue(owners.get(beforeSize + 2).getId() == owner3.getId());
    }

    @Test
    public void update() {
        ownerDao.add(owner);
        owner.setSurname("Kalinila");
        ownerDao.update(owner);
        Owner actual = ownerDao.getById(owner.getId());
        Assert.assertTrue(actual.getSurname().equals("Kalinila"));
    }

    @Test
    public void remove() {
        List<Owner> owners = ownerDao.getAll();
        int beforeSize = owners.size();
        ownerDao.add(owner);
        ownerDao.add(owner2);
        ownerDao.add(owner3);
        owners = ownerDao.getAll();
        Assert.assertTrue(owners.size() == beforeSize + 3);
        Assert.assertTrue(owners.contains(owner));
        ownerDao.remove(owner);
        owners = ownerDao.getAll();
        Assert.assertTrue(owners.size() == beforeSize + 2);
        Assert.assertFalse(owners.contains(owner));
    }

    @Test
    public void getAllOwnerDto() {
        List<Owner> owners = ownerDao.getAll();
        int beforeSize = owners.size();
        ownerDao.add(owner);
        ownerDao.add(owner2);
        ownerDao.add(owner3);
        car.setOwnerId(owner2.getId());
        car2.setOwnerId(owner.getId());
        car3.setOwnerId(owner2.getId());
        carDao.add(car);
        carDao.add(car2);
        carDao.add(car3);
        List<OwnerDto> ownerDtos = ownerDao.getAllOwnerDto();
        Assert.assertTrue(ownerDtos.size() == beforeSize + 3);
        Assert.assertTrue(ownerDtos.get(beforeSize).getOwner().equals(owner));
        Assert.assertTrue(ownerDtos.get(beforeSize + 1).getOwner().equals(owner2));
        Assert.assertTrue(ownerDtos.get(beforeSize + 2).getOwner().equals(owner3));
        Assert.assertTrue(ownerDtos.get(beforeSize).getCount() == 1);
        Assert.assertTrue(ownerDtos.get(beforeSize + 1).getCount() == 2);
        Assert.assertTrue(ownerDtos.get(beforeSize + 2).getCount() == 0);
    }
}