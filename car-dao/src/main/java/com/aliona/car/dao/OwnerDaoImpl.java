package com.aliona.car.dao;

import com.aliona.car.map.OwnerDtoMapper;
import com.aliona.car.map.OwnerMapper;
import com.aliona.car.model.Owner;
import com.aliona.car.model.OwnerDto;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.*;
import java.util.List;

public class OwnerDaoImpl implements OwnerDao {

    private final String SQL_INSERT_OWNER =
            "insert into owners_tbl (surname, name, birthday, passport) " +
                    "values (?, ?, ?, ?)";
    private final String SQL_SELECT_BY_ID_OWNER =
            "select * FROM owners_tbl WHERE id = ?";
    private final String SQL_SELECT_ALL_OWNERS = "select * FROM owners_tbl ";
    private final String SQL_UPDATE_BY_ID_OWNER = "UPDATE owners_tbl SET " +
            "surname = ?, name = ?, birthday = ?, passport = ? WHERE id = ?";
    private final String SQL_REMOVE_OWNER = "delete FROM owners_tbl WHERE id = ?";
    private final String SQL_GET_COUNT_CARS_OWNERS =
            "SELECT owners_tbl.*, COUNT(cars_tbl.id) AS carsCount FROM owners_tbl " +
                    "LEFT JOIN cars_tbl ON owners_tbl.id = cars_tbl.ownerId " +
                    "GROUP BY owners_tbl.id";

    private JdbcTemplate jdbcTemplate;
    private OwnerMapper ownerMapper;
    private OwnerDtoMapper ownerDtoMapper;

    public OwnerDaoImpl(JdbcTemplate jdbcTemplate, OwnerMapper ownerMapper, OwnerDtoMapper ownerDtoMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.ownerMapper = ownerMapper;
        this.ownerDtoMapper = ownerDtoMapper;
    }

    @Override
    public void add(Owner owner) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(SQL_INSERT_OWNER, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, owner.getSurname());
                ps.setString(2, owner.getName());
                ps.setDate(3, new java.sql.Date(owner.getBirthDate().getTime()));
                ps.setString(4, owner.getPassport());
                return ps;
            }
        }, keyHolder);

        int newCarId = keyHolder.getKey().intValue();
        owner.setId(newCarId);
    }

    @Override
    public Owner getById(Integer id) {
        List<Owner> owners = jdbcTemplate.query(SQL_SELECT_BY_ID_OWNER,
                new Object[]{id}, ownerMapper);
        if (owners.size() == 0)
            return null;
        else
            return owners.get(0);
    }

    @Override
    public List<Owner> getAll() {
        List<Owner> owners = jdbcTemplate.query(SQL_SELECT_ALL_OWNERS, ownerMapper);
        return owners;
    }

    @Override
    public void update(Owner owner) {
        Object[] params = new Object[]{owner.getSurname(), owner.getName()
                , owner.getBirthDate(), owner.getPassport(), owner.getId()};
        jdbcTemplate.update(SQL_UPDATE_BY_ID_OWNER, params);
    }

    @Override
    public void remove(Owner owner) {
        jdbcTemplate.update(SQL_REMOVE_OWNER, new Object[]{owner.getId()});
    }

    @Override
    public List<OwnerDto> getAllOwnerDto() {
        List<OwnerDto> ownerDtos = jdbcTemplate.query(SQL_GET_COUNT_CARS_OWNERS, ownerDtoMapper);
        return ownerDtos;
    }
}
