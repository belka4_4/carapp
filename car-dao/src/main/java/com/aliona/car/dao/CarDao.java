package com.aliona.car.dao;

import com.aliona.car.model.Car;

import java.util.List;

public interface CarDao {

    void add(Car car);

    Car getById(Integer id);

    List<Car> getAll();

    void update(Car car);

    void remove(Car car);
    
}
