package com.aliona.car.dao;

import com.aliona.car.model.Owner;
import com.aliona.car.model.OwnerDto;

import java.util.List;

public interface OwnerDao {

    void add(Owner owner);

    Owner getById(Integer id);

    List<Owner> getAll();

    void update(Owner owner);

    void remove(Owner owner);

    List<OwnerDto> getAllOwnerDto();

}
