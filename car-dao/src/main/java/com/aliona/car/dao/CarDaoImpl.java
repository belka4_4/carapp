package com.aliona.car.dao;

import com.aliona.car.map.CarMapper;
import com.aliona.car.model.Car;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class CarDaoImpl implements CarDao {

    private final String SQL_INSERT_CAR =
            "insert into cars_tbl (regNumber, model, vin, color, ownerId) " +
                    "values (?, ?, ?, ?, ?)";
    private final String SQL_SELECT_BY_ID_CAR =
            "select * FROM cars_tbl WHERE id = ?";
    private final String SQL_SELECT_ALL_CARS = "select * FROM cars_tbl ";
    private final String SQL_UPDATE_BY_ID_CAR = "UPDATE cars_tbl SET " +
            "regNumber = ?, model = ?, vin = ?, color = ?, ownerId = ? WHERE id = ?";
    private final String SQL_REMOVE_CAR = "delete FROM cars_tbl WHERE id = ?";

    private JdbcTemplate jdbcTemplate;
    private CarMapper carMapper;

    public CarDaoImpl(JdbcTemplate jdbcTemplate, CarMapper carMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.carMapper = carMapper;
    }

    @Override
    public void add(Car car) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(SQL_INSERT_CAR, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, car.getRegNumber());
                ps.setString(2, car.getModel());
                ps.setString(3, car.getVin());
                ps.setString(4, car.getColor());
                ps.setInt(5, car.getOwnerId());
                return ps;
            }
        }, keyHolder);

        int newCarId = keyHolder.getKey().intValue();
        car.setId(newCarId);
    }

    @Override
    public Car getById(Integer id) {
        List<Car> cars = jdbcTemplate.query(SQL_SELECT_BY_ID_CAR,
                new Object[]{id}, carMapper);
        if (cars.size() == 0)
            return null;
        else
            return cars.get(0);
    }

    @Override
    public List<Car> getAll() {
        List<Car> cars = jdbcTemplate.query(SQL_SELECT_ALL_CARS, carMapper);
        return cars;
    }

    @Override
    public void update(Car car) {
        Object[] params = new Object[]{car.getRegNumber(), car.getModel()
                , car.getVin(), car.getColor(), car.getOwnerId(), car.getId()};
        jdbcTemplate.update(SQL_UPDATE_BY_ID_CAR, params);
    }

    @Override
    public void remove(Car car) {
        jdbcTemplate.update(SQL_REMOVE_CAR, new Object[]{car.getId()});
    }
}
