package com.aliona.car.map;

import com.aliona.car.model.Owner;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class OwnerMapper implements RowMapper<Owner> {

    @Override
    public Owner mapRow(ResultSet resultSet, int i) throws SQLException {
        Owner owner = new Owner(
                resultSet.getString("surname"),
                resultSet.getString("name"),
                new Date(resultSet.getDate("birthday").getTime()),
                resultSet.getString("passport"));
        owner.setId(resultSet.getInt("id"));
        return owner;
    }
}
