package com.aliona.car.map;

import com.aliona.car.model.OwnerDto;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OwnerDtoMapper implements RowMapper<OwnerDto> {

    private OwnerMapper ownerMapper;

    public OwnerDtoMapper(OwnerMapper ownerMapper) {
        this.ownerMapper = ownerMapper;
    }

    @Override
    public OwnerDto mapRow(ResultSet resultSet, int i) throws SQLException {
        OwnerDto ownerDto = new OwnerDto(
                ownerMapper.mapRow(resultSet, i),
                resultSet.getInt("carsCount"));
        return ownerDto;
    }
}
