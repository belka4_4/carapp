package com.aliona.car.map;

import com.aliona.car.model.Car;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CarMapper implements RowMapper<Car> {

    @Override
    public Car mapRow(ResultSet resultSet, int i) throws SQLException {
        Car car = new Car(
                resultSet.getString("regNumber"),
                resultSet.getString("model"),
                resultSet.getString("vin"),
                resultSet.getString("color"),
                resultSet.getInt("ownerId"));
        car.setId(resultSet.getInt("id"));
        return car;
    }
}
