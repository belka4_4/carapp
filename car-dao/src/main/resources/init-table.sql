INSERT INTO owners_tbl (surname, name, birthday, passport)
                    values ('Ivanov', 'Ivan', '1973-12-18', '3240336');
INSERT INTO owners_tbl (surname, name, birthday, passport)
                    values ('Petrov', 'Petr', '1988-02-14', '3587631');
INSERT INTO owners_tbl (surname, name, birthday, passport)
                    values ('Sidorov', 'Mikhail', '1990-07-30', '3689521');
INSERT INTO cars_tbl (regNumber, model, vin, color, ownerId)
                    values ('1245HM1', 'lada', '1324567891012', 'red', 1);
INSERT INTO cars_tbl (regNumber, model, vin, color, ownerId)
                    values ('3232KK2', 'bmw', '3216549873578', 'blue', 2);
INSERT INTO cars_tbl (regNumber, model, vin, color, ownerId)
                    values ('5555EE1', 'mercedes', '0123456789362', 'black', 2)
