CREATE TABLE owners_tbl (
   id INTEGER IDENTITY,
   surname VARCHAR(30) NOT NULL,
   name VARCHAR(30) NOT NULL,
   birthday DATE NOT NULL,
   passport VARCHAR(9) NOT NULL,
   PRIMARY KEY (id)
);

CREATE TABLE cars_tbl (
   id INTEGER IDENTITY,
   regNumber VARCHAR(7) NOT NULL,
   model VARCHAR(50) NOT NULL,
   vin VARCHAR(17) NOT NULL,
   color VARCHAR(20) NOT NULL,
   ownerId INTEGER NOT NULL,
   PRIMARY KEY (id),
   FOREIGN KEY (ownerId) REFERENCES owners_tbl(id)
);